//console.log(`Hello world`)

//solution3 & 4
/*
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => {
    let titlesArr = data.map(element => {
        return element.title
    })
    
    titlesArr.forEach(element => {
        console.log(element)
    });
    
})
*/

//solution 5 & 6
/*
fetch(`https://jsonplaceholder.typicode.com/todos/3`)
.then(data => data.json())
.then(data => {
    console.log(`Title: ${data.title} \n Status: ${data.status}`);
})
*/

//solution 7
/*
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {"Content-type": "json/application"},
    body: JSON.stringify({
        userId: 1,
        title: "New Task",
        completed: false
    })
})
.then(data => data.json())
.then(data => {
    console.log(data);
})
*/

//solution 8
/*
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        userId: 4,
        title: "Updated Post",
        completed: true
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})
*/

//solution 9
/*
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post",
        description: "Test Description",
        status: "pending",
        dateCompleted: "",
        userId: 4,
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})
*/

//solution 10
/*
fetch(`https://jsonplaceholder.typicode.com/todos/5`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post thru Patch",
        userId: 4
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})
*/

//solution 11
/*
fetch(`https://jsonplaceholder.typicode.com/todos/7`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        status: "complete",
        date: "2022-03-07",
        completed: true
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})
*/

//solution 12
/*
fetch(`https://jsonplaceholder.typicode.com/posts/200`, {
    method: "DELETE"
})
*/
